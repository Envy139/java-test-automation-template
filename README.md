##This is my test repository template with two example tests

##Requirements:
- `Java 8`
- `Lombok`

WebDriver is from Windows os pointed to chrome version: `79.0.3945.36`.
To Run test on other os it requires to add other driver and change path in Config class.

##How to run test from console:
- Test Suite Example: `mvn test -DsuiteFile=src/main/resources/smokeSuite.xml`
- Single Test Example: `mvn -Dtest=LoginTest test`
- All Test: `mvn test`

###Parameters:
- `suiteFile` - it's path to suite file.
- `test` - it's name of test that you want to run.

