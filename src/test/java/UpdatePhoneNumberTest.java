import assertions.MyProfileAssertions;
import consumable.TestSetUp;
import org.testng.annotations.Test;
import pages.HomePage;
import scenarios.LoginScenario;

import static consumable.Config.getInstance;
import static org.apache.commons.lang3.RandomStringUtils.random;

public class UpdatePhoneNumberTest extends TestSetUp {

    private static final String RANDOM_PHONE_NUMBER = random(8, false, true);

    @Test
    public void updatePhoneNumber() {
        new HomePage(driver)
                .openHomePage()
                .clickMyAccountLink()
                .clickLoginLink()
                .run(new LoginScenario(getInstance().getDemoUserEmail(), getInstance().getDemoUserPassword()))
                .openMyProfileTab()
                .inputPhoneNumber(RANDOM_PHONE_NUMBER)
                .clickSubmitButton()
                .openMyProfileTab()
                .check(MyProfileAssertions.class)
                    .assertThatPhoneNumberIs(RANDOM_PHONE_NUMBER)
                .endAssertion();
    }
}
