import assertions.AccountAssertions;
import assertions.LoginAssertions;
import consumable.TestSetUp;
import org.testng.annotations.Test;
import pages.HomePage;

import static consumable.Config.getInstance;

public class LoginTest extends TestSetUp {

    private static final String DEMO_USERNAME = "Demo User";

    @Test
    public void loginAsDemoUser() {
        new HomePage(driver)
                .openHomePage()
                .clickMyAccountLink()
                .clickLoginLink()
                .check(LoginAssertions.class)
                    .assertThatLoginHeaderContainsLoginText()
                .endAssertion()
                .inputEmailAddress(getInstance().getDemoUserEmail())
                .inputPassword(getInstance().getDemoUserPassword())
                .clickLoginButton()
                .check(AccountAssertions.class)
                    .assertThatWelcomeMessageContainsUsername(DEMO_USERNAME)
                .endAssertion();
    }
}
