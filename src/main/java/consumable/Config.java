package consumable;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private static final Object lock = new Object();
    private static Config instance;
    private static Properties configFile;
    private static String environment;
    private static String configFilePath;

    private static String websiteUrl;
    private static String demoUserEmail;
    private static String demoUserPassword;

    public static Config getInstance() {
        if (instance == null) {
            synchronized (lock) {
                instance = new Config();
                configFile = new Properties();
                environment = System.getProperty("environment", "prod");
                setDriversPath();
                configFilePath = System.getProperty("user.dir") + "/src/main/resources/envs/" + environment + ".properties";
                instance.loadData();
            }
        }
        return instance;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public String getDemoUserEmail() {
        return demoUserEmail;
    }

    public String getDemoUserPassword() {
        return demoUserPassword;
    }

    private void loadData() {
        try {
            configFile.load(new FileInputStream(configFilePath));
        } catch (IOException e) {
            System.out.println("Configuration properties file cannot be found at: " + configFilePath);
        }
        websiteUrl = configFile.getProperty("websiteUrl");
        demoUserEmail = configFile.getProperty("demoUserEmail");
        demoUserPassword = configFile.getProperty("demoUserPassword");

    }

    private static void setDriversPath() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")  + "/src/main/resources/drivers/chromedriver.exe");
    }
}
