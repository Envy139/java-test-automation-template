package consumable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver driver;
    public WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new DefaultElementLocatorFactory(driver), this);
        wait = new WebDriverWait(driver, 15);
    }

    public void clickElementUsingJS(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", element);
    }

    @SuppressWarnings("unchecked")
    public <Input extends BasePage, Output extends BasePage> Output run(Scenario<Input, Output> scenario) {
        return scenario.run((Input) this);
    }

    @SuppressWarnings("unchecked")
    public <G extends BasePage, T extends AbstractAssertions<G>> T check(Class<T> clazz) throws RuntimeException {
        try {
            AbstractAssertions<G> assertions = clazz.newInstance();
            assertions.setPage((G)this);
            return (T) assertions;
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
