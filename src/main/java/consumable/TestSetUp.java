package consumable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static consumable.Config.getInstance;
import static java.util.concurrent.TimeUnit.SECONDS;

public class TestSetUp {

    protected WebDriver driver;

    @BeforeMethod
    public void launchBrowser() {
        getInstance();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, SECONDS);
    }

    @AfterMethod
    public void closeBrowser() {
        driver.close();
        driver.quit();
    }
}
