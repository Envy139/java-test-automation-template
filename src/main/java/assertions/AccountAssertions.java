package assertions;

import consumable.AbstractAssertions;
import lombok.extern.log4j.Log4j;
import pages.AccountPage;

import static org.testng.Assert.assertTrue;

@Log4j
public class AccountAssertions extends AbstractAssertions<AccountPage> {

    public AccountAssertions assertThatWelcomeMessageContainsUsername(String username) {
        assertTrue(page.getWelcomeMessage().getText().contains(username), "Welcome Message doesn't contain username: " + username);
        log.info("Successfully verified that welcome message contains username: " + username);
        return this;
    }
}
