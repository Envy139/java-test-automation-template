package assertions;

import consumable.AbstractAssertions;
import lombok.extern.log4j.Log4j;
import pages.MyProfilePage;

import static org.testng.Assert.assertEquals;

@Log4j
public class MyProfileAssertions extends AbstractAssertions<MyProfilePage> {

    public MyProfileAssertions assertThatPhoneNumberIs(String phoneNumber) {
        assertEquals(phoneNumber, page.getPhoneInput().getAttribute("value"), "Phone number isn't equal: " + phoneNumber);
        log.info("Successfully verified that phone number is: " + phoneNumber);
        return this;
    }
}
