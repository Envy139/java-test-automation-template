package assertions;

import consumable.AbstractAssertions;
import lombok.extern.log4j.Log4j;
import pages.LoginPage;

import static org.testng.Assert.assertTrue;

@Log4j
public class LoginAssertions extends AbstractAssertions<LoginPage> {

    public LoginAssertions assertThatLoginHeaderContainsLoginText() {
        assertTrue(page.getLoginHeaderLink().getText().contains("Login"), "Login header doesn't contain Login text");
        log.info("Successfully verified that login header contains login text");
        return this;
    }
}
