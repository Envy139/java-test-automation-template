package pages;

import consumable.BasePage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class MyProfilePage extends BasePage {

    public static final String ACTIVE_BOOKING_TAB_CSS_SELECTOR = "[href='#bookings'].active";

    @Getter
    @FindBy(css = "[name='phone']")
    private WebElement phoneInput;

    @FindBy(css = ".btn.btn-block.updateprofile.btn-primary")
    private WebElement submitButton;

    public MyProfilePage(WebDriver driver) {
        super(driver);
    }

    public MyProfilePage inputPhoneNumber(String phoneNumber) {
        phoneInput.clear();
        phoneInput.sendKeys(phoneNumber);
        return this;
    }

    public AccountPage clickSubmitButton() {
        submitButton.click();
        wait.until(presenceOfElementLocated(cssSelector(ACTIVE_BOOKING_TAB_CSS_SELECTOR)));
        return new AccountPage(driver);
    }
}
