package pages;

import consumable.BasePage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class AccountPage extends BasePage {

    @Getter
    @FindBy(className = "text-align-left")
    private WebElement welcomeMessage;

    @FindBy(css = "div.imagelogo")
    private WebElement phpTravelsLogo;

    @FindBy(css = "[href='#profile']")
    private WebElement myProfileLink;

    public AccountPage(WebDriver driver) {
        super(driver);
        wait.until(visibilityOf(phpTravelsLogo));
    }

    public MyProfilePage openMyProfileTab() {
        myProfileLink.click();
        return new MyProfilePage(driver);
    }
}
