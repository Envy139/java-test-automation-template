package pages;

import consumable.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static consumable.Config.getInstance;

public class HomePage extends BasePage {

    @FindBy(css = ".dropdown-login #dropdownCurrency")
    private WebElement myAccountLink;

    @FindBy(css = ".dropdown-item.active.tr")
    private WebElement loginLink;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage openHomePage() {
        driver.get(getInstance().getWebsiteUrl());
        return this;
    }

    public HomePage clickMyAccountLink() {
        myAccountLink.click();
        return this;
    }

    public LoginPage clickLoginLink() {
        loginLink.click();
        return new LoginPage(driver);
    }
}
